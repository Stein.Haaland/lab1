package rockPaperScissors;

import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> validContinueAnswers = Arrays.asList("y","n");


    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true){
        System.out.println("Let's play round " + roundCounter);
        String humanChoice = userChoice();
        String computerChoice = randomChoice();
        roundCounter += 1;
        String choiceString = ("Human chose " + humanChoice + ", computer chose " + computerChoice +".") ;
        
        if(isWinner(humanChoice, computerChoice)){
            System.out.println(choiceString + " Human wins!");
            humanScore +=1 ;
        }
        else if(isWinner(computerChoice, humanChoice)){
            System.out.println(choiceString + " Computer wins!");
            computerScore +=1 ;
        }
        else{
            System.out.println(choiceString + " It's a tie!" );
        }
        System.out.println("Score: human " + humanScore + ", computer " + computerScore );
        if (continuePlaying().equals("n")){
            break;
    
        }
       
        }
    System.out.println("Bye bye :)");
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }  
    public String userChoice(){
        while(true){
        String humansChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        if (validateInput(humansChoice, rpsChoices)){
            return humansChoice;
        }
         else{
             System.out.println("I don't understand" + humansChoice +". Try again");
            }  
        }
    }
    public Boolean validateInput(String input, List<String> validInput){
        input = input.toLowerCase();
        return (validInput.contains(input));
    }
    public String randomChoice(){
        Random rand = new Random();
        return (rpsChoices.get(rand.nextInt(rpsChoices.size())));
    }
    public Boolean isWinner(String choice1, String choice2){
        if(choice1.equals("paper")){
            return (choice2.equals("rock"));
        }
        else if(choice1.equals("scissors")){
            return(choice2.equals("paper"));
        }
        else {
            return(choice2.equals("scissors"));
        }
    }
    public String continuePlaying(){
        while(true){
            String countineAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validateInput(countineAnswer, validContinueAnswers)){
                return(countineAnswer);
            }
            else{
                System.out.println("I don't understand" + countineAnswer + ". Try again");
            }
        }
    }


}
